import unittest
import algorithm

from node import Node


class AlgorithmTest(unittest.TestCase):
    def test_visit(self):
        visit_queue = []
        node_1 = Node((-1, -1), (1, 2), 0, 0)
        node_2 = Node((-1, -1), (2, 3), 0, 0)
        algorithm.visit(node_1, visit_queue)
        algorithm.visit(node_2, visit_queue)

        self.assertEqual(visit_queue, [(1, 2), (2, 3)])

    def test_is_visited(self):
        visit_queue = [(1, 2), (5, 4), (3, 2)]

        self.assertIs(algorithm.is_visited((5, 4), visit_queue), True)
        self.assertIs(algorithm.is_visited((5, 3), visit_queue), False)

    def test_is_goal(self):
        self.assertIs(algorithm.is_goal(
            Node((-1, -1), (1, 2), 0, 0), (1, 2)), True)
        self.assertIs(algorithm.is_goal(
            Node((-1, -1), (1, 2), 0, 0), (1, 3)), False)

    def test_mark_backtrack(self):
        backtrack_queue = {}
        algorithm.mark_backtrack(Node((-1, -1), (1, 2), 0, 0), backtrack_queue)
        algorithm.mark_backtrack(Node((1, 2), (3, 4), 0, 0), backtrack_queue)
        self.assertEqual(backtrack_queue[(1, 2)], (-1, -1))
        self.assertEqual(backtrack_queue[(3, 4)], (1, 2))

    def test_get_path(self):
        backtrack_queue = {}
        backtrack_queue[(0, 0)] = (-1, -1)
        backtrack_queue[(1, 1)] = (0, 0)
        backtrack_queue[(2, 2)] = (1, 1)

        self.assertEqual(algorithm.get_path(
            (2, 2), backtrack_queue), [(0, 0), (1, 1), (2, 2)])

    def test_euclidean_distance(self):
        calculate_distance = algorithm.euclidean_distance((3, 3))

        self.assertAlmostEqual(calculate_distance((1, 1)), 2.8284271)

    def test_filter_valid_neighbors(self):
        neighbors = ((1, 0), (-1, 0), (0, 1), (0, -1),
                     (1, 1), (-1, 1), (1, -1), (-1, -1))

        graph = [[0, 1, 0, 0, 0],
                 [0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0]]

        self.assertEqual(algorithm.filter_valid_neighbors(
            neighbors, graph), ((1, 0), (1, 1)))

    def test_get_all_neighbors(self):
        current = Node((-1, -1), (2, 2), 0, 0)

        graph = [[0, 0, 0, 0, 0],
                 [0, 1, 0, 1, 0],
                 [0, 0, 0, 0, 0],
                 [0, 0, 1, 1, 0],
                 [0, 0, 0, 0, 0]]

        self.assertEqual(algorithm.get_all_neighbors(
            current, graph), ((1, 2), (2, 3), (2, 1), (3, 1)))
