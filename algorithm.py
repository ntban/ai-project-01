import frontier
import cost_strategy
import random
import math

from node import Node


def visit(node, visit_queue):
    visit_queue.append(node.index)


def is_visited(node_index, visit_queue):
    return node_index in visit_queue


def is_goal(node, goal):
    return node.index == goal


def add_neighbors_to_frontier(node, neighbors, frontier, heuristics_func):
    nodes = [
        Node(
            parent=node.index,
            index=neighbor,
            cost=node.cost + 1,
            final_cost=node.cost + 1 + heuristics_func(neighbor))
        for neighbor in neighbors
    ]
    frontier.push(nodes)


def mark_backtrack(node, backtrack_queue):
    backtrack_queue[node.index] = node.parent


def get_path(goal, backtrack_queue):
    path = []
    path.append(goal)

    prev = backtrack_queue[goal]
    while prev != (-1, -1):
        path.append(prev)
        prev = backtrack_queue[prev]

    path.reverse()
    return path


def get_answer(arr):
    ans = ''
    for i in range(len(arr)):
        ans += str(arr[i])
        if (i < len(arr) - 1):
            ans += ' '
    return ans


def euclidean_distance(goal):
    def calculate_distance(start):
        return math.sqrt((start[0] - goal[0])**2 + (start[1] - goal[1])**2)

    return calculate_distance


def filter_valid_neighbors(neighbors, graph):
    limit_x = len(graph)
    limit_y = 0
    if limit_x > 0:
        limit_y = len(graph[0])

    def within_limit(node):
        return node[0] >= 0 and node[0] < limit_x and node[1] >= 0 and node[1] < limit_y and graph[node[0]][node[1]] == 0

    return tuple((item for item in neighbors if within_limit(item)))


def get_all_neighbors(current, graph):
    x = current.index[0]
    y = current.index[1]

    neighbors = ((x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1),
                 (x + 1, y + 1), (x - 1, y + 1), (x + 1, y - 1), (x - 1, y - 1))
    neighbors = filter_valid_neighbors(neighbors, graph)

    return neighbors


def base_algorithm(graph, start, goal, frontier, heuristic_func):
    visit_queue = []
    backtrack_queue = {}

    frontier.push([Node((-1, -1), start, 0, heuristic_func(start))])
    while not frontier.empty():
        next = frontier.pop()

        # when G is popped out
        if is_goal(next, goal):
            visit(next, visit_queue)
            mark_backtrack(next, backtrack_queue)
            break

        # when the popped node is visited
        if is_visited(next.index, visit_queue):
            continue

        # mark the node as visited
        visit(next, visit_queue)
        # mark backtrack path for next
        mark_backtrack(next, backtrack_queue)
        # get all neighbors of current node
        neighbors = get_all_neighbors(next, graph)
        # add all of its neighbors to frontier
        add_neighbors_to_frontier(
            next, neighbors, frontier, heuristic_func)

    # print out the execution order
    exe_order = visit_queue

    # check if goal is found
    if is_visited(goal, visit_queue):
        is_found = backtrack_queue
    else:
        is_found = -1

    return [exe_order, is_found]


def a_star(graph, start, goal):
    return base_algorithm(graph, start, goal, frontier.PriorityQueueFrontier(), euclidean_distance(goal))


def run(graph, start, goal):
    return a_star(graph, start, goal)
