def display_result(start, goal, optimal_path, graph):
    if optimal_path == -1:
        return '-1'

    optimal_map = []
    for x in range(len(graph)):
        optimal_map.append([])
        for y in range(len(graph[x])):
            if graph[x][y] == 0:
                optimal_map[x].append('-')
            else:
                optimal_map[x].append('o')

    answer = str(len(optimal_path)) + \
        ' # the number of steps to reach the goal.\n'
    for step in optimal_path:
        answer += str(step) + ' '

        step_on_map = 'x'
        if step == start:
            step_on_map = 'S'
        elif step == goal:
            step_on_map = 'G'
        optimal_map[step[0]][step[1]] = step_on_map

    answer += '\n# the position of each step in the optimal path.\n# the map with the optimal path.'
    for x in range(len(optimal_map)):
        answer += '\n'
        for y in range(len(optimal_map[x])):
            answer += optimal_map[x][y] + ' '

    return answer
