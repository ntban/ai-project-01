from collections import deque
import heapq


class Frontier:

    def push(self, node):
        pass

    def pop(self):
        pass

    def empty(self):
        pass


class PriorityQueueFrontier(Frontier):

    pqueue = []
    count = 0

    def push(self, nodes):
        for node in nodes:
            heapq.heappush(self.pqueue, (node.final_cost, self.count, node))
            self.count += 1

    def pop(self):
        return heapq.heappop(self.pqueue)[2]

    def empty(self):
        return len(self.pqueue) == 0
