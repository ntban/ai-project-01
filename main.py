import algorithm
import display

if __name__ == "__main__":
    # read input
    file = open('input.txt', 'r')

    lines = [line.replace('\n', '').replace('\r', '')
             for line in file.readlines()]

    size_x, size_y = [int(num) for num in lines[0].split(' ')]
    start = tuple((int(num) for num in lines[1].split(' ')))
    goal = tuple((int(num) for num in lines[2].split(' ')))

    graph = []
    for i in range(size_x):
        graph.append([int(num) for num in lines[i + 3].split(' ')])

    file.close()

    # run algorigthm
    print('Running...')
    exe_order, is_found = algorithm.run(graph, start, goal)

    answer = display.display_result(start, goal, is_found, graph)

    # write output
    file = open('output.txt', 'w')
    file.write(answer)
    file.close()
    print('Done!')
